Author: Harry Cheng

Repo: https://bitbucket.org/harrycheng5/proj4-brevets/

Description: It is a web app that can compute the open time and close time for brevet ride based on the control points user has put in. For the rule of brevet, you can check here (https://rusa.org/pages/rulesForRiders). 

For users to start, you need to select the length of brevet (200, 300, 400, 600, 1000 km) and starting date and time. After that, all you need to do is enter the control points on the website. 

For developers, the knowledges behind this project are AJAX and Flask. AJAX is responsible for the client side where it controls what users see on the website. It also sends the needed data back and forth between the client and server and updates the information on the website at the same time. Flask is implemented in the server side where all our algorithms are computed there. The rule of our algorithms is based on here (https://rusa.org/pages/acp-brevet-control-times-calculator). 

The main algorithm used in this project is in the two functions in acp_times.py
open_time(control_dist_km, brevet_dist_km, brevet_start_time)
close_time(control_dist_km, brevet_dist_km, brevet_start_time)

To test acp_time.py, please run nosetests in the docker container. See specific instruction below:

- Run the command to set up an interactive container

$ docker run --entrypoint/bin/bash -i-t <image_name> 

 - run nosetest command. The test cases were already built in tests/test_acp_times.py

$ nosetests