"""
Nose tests for acp_times.py
"""

from acp_times import open_time, close_time

import nose
import arrow
import logging
logging.basicConfig()
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_simple():
    """
    control km is smaller than brevet km,
    and brevet km is smaller than 200 (using only one speed in the table)
    """

    assert close_time(60, 200, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 04:00").isoformat()
    assert open_time(60, 200, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 01:46").isoformat()

def test_simple2():
    """
    control km longer than brevet 200
    """
    assert close_time(205, 200, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 13:30").isoformat()
    assert open_time(205, 200, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 05:53").isoformat()


def test_multiple_speeds():
    """
    use multiple rows in the speed table
    """
    assert close_time(350, 600, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 23:20").isoformat()
    assert open_time(350, 600, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 10:34").isoformat()

def test_multiple_speeds2():
    """
    use multiple rows in the speed table
    control is longer than brevet
    """
    assert close_time(609, 600, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-02 16:00").isoformat()
    assert open_time(609, 600, arrow.get("2017-01-01 00:00")) == (arrow.get("2017-01-01 18:48").isoformat())

def test_0km():
    """
    it should work even when input is 0
    """
    assert close_time(0, 600, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 00:00").isoformat()
    assert open_time(0, 600, arrow.get("2017-01-01 00:00")) == arrow.get("2017-01-01 00:00").isoformat()

